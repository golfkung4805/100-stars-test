import type { Metadata } from 'next';
import { Noto_Sans } from 'next/font/google';

import '@/config/dayjs.config';

import './globals.css';
import { cn } from '@/utils';

const notoSans = Noto_Sans({ subsets: ['latin'], weight: ['400', '500', '700'], variable: '--font-noto-sans' });

export const metadata: Metadata = {
  title: '1000 Stars',
  description: '1000 Stars - Test CRUD',
};

const RootLayout = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) => {
  return (
    <html
      lang="en"
      className="scroll-smooth"
    >
      <body
        className={cn(
          notoSans.className,
          'text-neutral-900 bg-neutral-300 relative mx-auto max-w-screen-xl overflow-hidden',
        )}
      >
        {children}
      </body>
    </html>
  );
};

export default RootLayout;
