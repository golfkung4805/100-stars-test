import React from 'react';

import FormEmployee from '@/components/container/employee/FormEmployee';
import * as employeeService from '@/services/employee.services';
import { notFound } from 'next/navigation';

interface IProps {
  params: {
    id: string;
  };
}

const Page = async ({ params: { id } }: IProps) => {
  const employee = await employeeService.getById(id);
  if (!employee) {
    return notFound();
  }

  return <FormEmployee initialValues={employee} />;
};

export default Page;
