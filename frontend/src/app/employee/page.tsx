import React from 'react';

import EmployeeList from '@/components/container/employee/List';
import * as employeeService from '@/services/employee.services';
const Page = async () => {
  const employeeLists = await employeeService.getAll({});
  return <EmployeeList employeeLists={employeeLists} />;
};

export default Page;
