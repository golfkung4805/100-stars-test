'use client';

import { StyleProvider } from '@ant-design/cssinjs';
import { AntdRegistry } from '@ant-design/nextjs-registry';
import { ConfigProvider } from 'antd';
import thTh from 'antd/locale/th_TH';

import '@/config/dayjs.config';

const Template = ({ children }: { children: React.ReactNode }) => {
  return (
    <AntdRegistry>
      <ConfigProvider
        locale={thTh}
        theme={{
          token: {
            fontSize: 16,
            fontFamily: 'var(--font-noto-sans)',
          },
        }}
      >
        <StyleProvider hashPriority="low">
          <div className="relative flex h-dvh flex-col bg-white overflow-hidden">{children}</div>
        </StyleProvider>
      </ConfigProvider>
    </AntdRegistry>
  );
};

export default Template;
