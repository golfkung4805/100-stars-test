import axios, { AxiosRequestConfig } from 'axios';

import { ENDPOINT_CORE_API } from '@/constants/env';

const client = axios.create({
  baseURL: ENDPOINT_CORE_API,
  headers: {
    'Content-Type': 'application/json',
  },
});

class DataService {
  static get(path = '') {
    return client({
      method: 'GET',
      url: path,
    });
  }

  static post(path = '', data = {}, optionalHeader = {}, config = {}) {
    return client({
      method: 'POST',
      url: path,
      data,
      headers: { ...optionalHeader },
    });
  }

  static patch(path = '', data = {}) {
    return client({
      method: 'PATCH',
      url: path,
      data: JSON.stringify(data),
    });
  }

  static put(path = '', data = {}) {
    return client({
      method: 'PUT',
      url: path,
      data: JSON.stringify(data),
    });
  }

  static delete(path = '') {
    return client({
      method: 'DELETE',
      url: path,
    });
  }
}

export { DataService };
