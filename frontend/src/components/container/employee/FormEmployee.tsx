'use client';

import { useRouter } from 'next/navigation';
import React, { Fragment, useEffect, useMemo, useState } from 'react';
import dayjs from 'dayjs';
import { Button, Col, DatePicker, Flex, Form, Input, Radio, Row, Select, notification } from 'antd';
import type { DefaultOptionType } from 'antd/es/select';

import useAutoCompleteAddressThai from '@/hooks/useAutoCompleteAddressThai';

import * as employeeServices from '@/services/employee.services';
import type { EmployeeInfo } from '@/services/employee.services';
import * as typeServices from '@/services/types.services';

interface FormEmployee {
  initialValues?: EmployeeInfo;
}

const FormEmployee = ({ initialValues }: FormEmployee) => {
  const router = useRouter();
  const [form] = Form.useForm();
  const autoCompleteAddress = useAutoCompleteAddressThai();

  const formMode = useMemo(() => (initialValues ? 'แก้ไข' : 'เพิ่ม'), [initialValues]);
  const [initLoading, setInitLoading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [genderOptions, setGenderOptions] = useState<{
    loading: boolean;
    lists: DefaultOptionType[];
  }>({
    loading: false,
    lists: [],
  });

  const provinceId = Form.useWatch(['provinceId'], form);
  const districtId = Form.useWatch(['districtId'], form);

  const handleSubmit = () => form.submit();

  const onFinish = async (value: EmployeeInfo) => {
    try {
      setLoading(true);
      if (initialValues) {
        await employeeServices.update(initialValues.id, value);
      } else {
        await employeeServices.create(value);
      }
      notification.success({
        message: 'แจ้งเตือน',
        description: `บันทึกข้อมูลพนักงานสำเร็จ`,
      });
      router.push('/employee');
      router.refresh();
    } catch {
      notification.error({
        message: 'แจ้งเตือน',
        description: `บันทึกข้อมูลพนักงานไม่สำเร็จ`,
      });
    } finally {
      setLoading(false);
    }
  };

  const initMaster = async () => {
    setInitLoading(true);
    try {
      setGenderOptions((prev) => ({ ...prev, loading: true }));
      const genderOptions = typeServices.getGender();
      setGenderOptions((prev) => ({ ...prev, lists: genderOptions, loading: false }));
    } catch (error) {
      notification.error({ message: 'พบข้อผิดพลาด', description: 'ไม่สามารถเรียกข้อมูลสำหรับใช้ในการเพิ่มข้อมูลได้' });
    } finally {
      setInitLoading(false);
    }
  };

  useEffect(() => {
    initMaster();
  }, []);

  useEffect(() => {
    if (initialValues) {
      form?.setFieldsValue({
        ...initialValues,
        birthday: dayjs.utc(initialValues?.birthday),
        id_card_expiry: dayjs.utc(initialValues?.id_card_expiry),
      });
      autoCompleteAddress.onSelectProvince(initialValues.province);
      autoCompleteAddress.onSelectDistrict(initialValues.district);
    }
  }, [initialValues]);

  useEffect(() => {
    if (provinceId) autoCompleteAddress.onSelectProvince(provinceId);
    if (districtId) autoCompleteAddress.onSelectDistrict(districtId);
  }, [provinceId, districtId]);

  return (
    <div className="px-5 sm:px-10 py-8 space-y-5 flex-1 overflow-auto">
      <h1 className="text-xl font-semibold">{formMode}ข้อมูลพนักงาน</h1>
      <Form
        layout="vertical"
        form={form}
        onFinish={onFinish}
        initialValues={{ hasSpecificReceiver: false, isSpecificAddress: false }}
        disabled={loading || initLoading}
      >
        <Row
          align="middle"
          justify="center"
          gutter={[10, 10]}
        >
          <Col
            xs={24}
            sm={6}
            md={4}
          >
            <Form.Item
              name="gender"
              label="คำนำหน้า"
              rules={[{ required: true }]}
            >
              <Select
                size="large"
                loading={genderOptions.loading}
                options={genderOptions.lists}
              />
            </Form.Item>
          </Col>
          <Col
            xs={24}
            sm={9}
            md={10}
          >
            <Form.Item
              name="first_name"
              label="ชื่อจริง"
              rules={[{ required: true }]}
            >
              <Input size="large" />
            </Form.Item>
          </Col>

          <Col
            xs={24}
            sm={9}
            md={10}
          >
            <Form.Item
              name="last_name"
              label="นามสกุล"
              rules={[{ required: true }]}
            >
              <Input size="large" />
            </Form.Item>
          </Col>
          <Col
            xs={24}
            sm={12}
          >
            <Form.Item
              name="birthday"
              label="วันเกิด"
              rules={[{ required: true }]}
            >
              <DatePicker
                inputReadOnly
                className="!w-full"
                format="DD MMM YYYY"
                size="large"
              />
            </Form.Item>
          </Col>
          <Col
            xs={24}
            sm={12}
          >
            <Form.Item
              name="id_card_expiry"
              label="วันหมดอายุบัตรประชาชน"
              rules={[{ required: true }]}
            >
              <DatePicker
                inputReadOnly
                className="!w-full"
                format="DD MMM YYYY"
                size="large"
              />
            </Form.Item>
          </Col>
          <Col xs={24}>
            <Form.Item
              name="address"
              label="ที่อยู่"
              rules={[{ required: true }]}
            >
              <Input size="large" />
            </Form.Item>
          </Col>
          <Col xs={12}>
            <Form.Item
              name="province"
              label="จังหวัด"
              rules={[{ required: true }]}
            >
              <Select
                size="large"
                loading={autoCompleteAddress.loading.province}
                options={autoCompleteAddress.province}
                onChange={(value) => {
                  form?.setFieldValue('districtId', '');
                  form?.setFieldValue('subdistrictId', '');
                  form?.setFieldValue('postCode', '');
                  autoCompleteAddress.onSelectProvince(value);
                }}
              />
            </Form.Item>
          </Col>
          <Col xs={12}>
            <Form.Item
              name="district"
              label="อำเภอ/เขต"
              rules={[{ required: true }]}
            >
              <Select
                size="large"
                loading={autoCompleteAddress.loading.district}
                options={autoCompleteAddress.district}
                onChange={(value) => {
                  form?.setFieldValue('subdistrictId', '');
                  form?.setFieldValue('postCode', '');
                  autoCompleteAddress.onSelectDistrict(value);
                }}
              />
            </Form.Item>
          </Col>
          <Col xs={12}>
            <Form.Item
              name="sub_district"
              label="ตำบล/แขวง"
              rules={[{ required: true }]}
            >
              <Select
                size="large"
                loading={autoCompleteAddress.loading.subDistrict}
                options={autoCompleteAddress.subDistrict}
              />
            </Form.Item>
          </Col>
          <Col xs={12}>
            <Form.Item
              name="zip_code"
              label="รหัสไปรษณีย์"
              rules={[
                { required: true, max: 5 },
                {
                  pattern: new RegExp('^[0-9]+$'),
                  message: 'รหัสไปรษณีย์จะต้องเป็นตัวเลขเท่านั้น',
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
          </Col>
          <Col xs={24}>
            <Button
              className="float-start"
              size="large"
              onClick={() => router.push('/employee')}
            >
              ย้อนกลับ
            </Button>
            <Button
              className="float-end"
              type="primary"
              size="large"
              onClick={handleSubmit}
            >
              บันทึกข้อมูล
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default FormEmployee;
