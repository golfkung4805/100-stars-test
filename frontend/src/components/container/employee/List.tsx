'use client';

import { useRouter } from 'next/navigation';
import React, { useEffect, useMemo, useState } from 'react';
import Link from 'next/link';
import dayjs from 'dayjs';
import fileDownload from 'js-file-download';
import { Button, DatePicker, Dropdown, Form, Input, Modal, notification, Select, Table } from 'antd';
import { DeleteOutlined, EditOutlined, MoreOutlined, ExclamationCircleFilled } from '@ant-design/icons';
import type { TableColumnsType } from 'antd';
import type { DefaultOptionType } from 'antd/es/select';

import * as employeeServices from '@/services/employee.services';
import type { EmployeeInfo, FilterGetAll } from '@/services/employee.services';
import { GenderType } from '@/services/types.services';
import * as typeServices from '@/services/types.services';

interface IList {
  employeeLists: EmployeeInfo[];
}

const List = ({ employeeLists }: IList) => {
  const router = useRouter();
  const [dataEmployeeLists, setDataEmployeeLists] = useState(employeeLists);
  const [idCardOptions, setFilterIdCardOptions] = useState<{
    loading: boolean;
    lists: DefaultOptionType[];
  }>({
    loading: false,
    lists: [],
  });
  const handleExportCsv = async () => {
    try {
      const data = await employeeServices.exportCsv({});
      fileDownload(data, 'export-csv.csv');
      notification.success({
        message: 'แจ้งเตือน',
        description: `ส่งออกไฟล์ .CSV สำเร็จ`,
      });
    } catch (error) {
      notification.error({ message: 'พบข้อผิดพลาด', description: 'ไม่สามารถส่งออกไฟล์ .CSV ได้' });
    }
  };
  const handleDelete = async (employeeInfo: EmployeeInfo) => {
    Modal.confirm({
      title: `แจ้งเตือน`,
      icon: <ExclamationCircleFilled />,
      content: `คุณต้องการลบข้อมูลพนักงาน "${employeeInfo.first_name} ${employeeInfo.last_name}" หรือไม่?`,
      onOk: async () => {
        try {
          await employeeServices.del(employeeInfo.id);
          notification.success({
            message: 'แจ้งเตือน',
            description: `ลบข้อมูลพนักงาน "${employeeInfo.first_name} ${employeeInfo.last_name}" สำเร็จ`,
          });
          router.refresh();
        } catch (error) {
          notification.error({ message: 'พบข้อผิดพลาด', description: 'ไม่สามารถลบข้อมูลพนักงานได้' });
        }
      },
    });
  };

  const columns: TableColumnsType<EmployeeInfo> = [
    {
      title: 'ชื่อ - สกุล',
      dataIndex: 'name',
      key: 'name',
      width: 250,
      render: (_, record) => `${GenderType[record.gender]}${record.first_name} ${record.last_name}`,
    },
    {
      title: 'วันเกิด',
      dataIndex: 'birthday',
      key: 'birthday',
      width: 180,
      render: (text) => `${text ? dayjs.utc(text).format('DD MMMM BBBB') : '-'}`,
    },
    {
      title: 'ที่อยู่',
      dataIndex: 'address',
      key: 'address',
      render: (text, record) =>
        `${text} ${record.thai_tambons?.name_th}, ${record.thai_amphures?.name_th}, ${record.thai_provinces?.name_th} ${record.zip_code}`,
    },
    {
      title: 'วันหมดอายุบัตร',
      dataIndex: 'id_card_expiry',
      key: 'id_card_expiry',
      width: 180,
      render: (text) => `${text ? dayjs.utc(text).format('DD MMMM BBBB') : '-'}`,
    },
    {
      title: 'ตัวเลือก',
      key: 'action',
      align: 'center',
      width: 100,
      render: (_, record) => {
        return (
          <Dropdown
            menu={{
              items: [
                {
                  key: 'update',
                  label: (
                    <Link href={`/employee/${record.id}`}>
                      <Button
                        type="link"
                        icon={<EditOutlined />}
                        className="!text-neutral-900"
                      >
                        แก้ไขข้อมูลพนักงาน
                      </Button>
                    </Link>
                  ),
                },
                {
                  key: 'delete',
                  label: (
                    <Button
                      type="link"
                      icon={<DeleteOutlined />}
                      className="!text-neutral-900"
                      onClick={() => handleDelete(record)}
                    >
                      ลบข้อมูลพนักงาน
                    </Button>
                  ),
                },
              ],
            }}
            placement="bottomRight"
            arrow
          >
            <Button
              size="large"
              icon={<MoreOutlined />}
            />
          </Dropdown>
        );
      },
    },
  ];

  const onFilter = async (values: FilterGetAll) => {
    try {
      const res = await employeeServices.getAll({
        ...values,
        monthOfBirthday: dayjs(values.monthOfBirthday).get('month') + 1,
      });
      setDataEmployeeLists(res);
    } catch (error) {
      notification.error({ message: 'พบข้อผิดพลาด', description: 'ไม่สามารถกรองข้อมูลได้' });
    }
  };

  const initMaster = async () => {
    try {
      setFilterIdCardOptions((prev) => ({ ...prev, loading: true }));
      const idCardOptions = typeServices.getFilterCardIdExpiry();
      setFilterIdCardOptions((prev) => ({ ...prev, lists: idCardOptions, loading: false }));
    } catch (error) {
      notification.error({ message: 'พบข้อผิดพลาด', description: 'ไม่สามารถเรียกข้อมูลสำหรับกรองข้อมูลได้' });
    } finally {
      setFilterIdCardOptions((prev) => ({ ...prev, loading: false }));
    }
  };

  useEffect(() => {
    initMaster();
  }, []);

  return (
    <div className="employee-list flex-1 px-12 py-8 space-y-5 overflow-auto">
      <div className="flex flex-col md:flex-row gap-3 justify-between items-center">
        <h1 className="text-xl font-semibold">รายการพนักงาน</h1>
        <div className="flex flex-col md:flex-row gap-3 ">
          <Button
            size="large"
            onClick={() => handleExportCsv()}
          >
            ส่งออกไฟล์ .CSV
          </Button>
          <Link href="/employee/create">
            <Button
              type="primary"
              size="large"
            >
              เพิ่มข้อมูลพนักงาน
            </Button>
          </Link>
        </div>
      </div>
      <div className="flex items-center space-x-3">
        <h1 className="text-lg font-semibold">ตัวกรอง</h1>
        <div className="filter flex items-center">
          <Form
            layout="inline"
            // initialValues={{ cardIdExpiry: '' }}
            onFinish={onFilter}
          >
            <Form.Item
              className="!mb-0"
              name="monthOfBirthday"
            >
              <DatePicker
                rootClassName="[&_.ant-picker-header]:!hidden"
                className="!w-60"
                format="MMMM"
                picker="month"
                placeholder="กรองด้วยวันเกิด"
              />
            </Form.Item>
            <Form.Item
              className="!mb-0"
              name="cardIdExpiry"
            >
              <Select
                className="!w-60"
                placeholder="กรองด้วยวันหมดอายุบัตร"
                options={idCardOptions.lists}
              />
            </Form.Item>
            <Button
              type="primary"
              htmlType="submit"
            >
              ค้นหา
            </Button>
          </Form>
        </div>
      </div>
      <Table
        rowKey={(row) => row.id}
        bordered
        size="large"
        dataSource={dataEmployeeLists}
        columns={columns}
        scroll={{ x: '100%' }}
      />
    </div>
  );
};

export default List;
