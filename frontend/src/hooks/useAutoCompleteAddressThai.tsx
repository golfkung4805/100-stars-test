'use client';

import { useEffect, useState } from 'react';

import * as service from '@/services/useAutoCompleteAddressThai.services';
import { DefaultOptionType } from 'antd/es/select';

const useAutoCompleteAddressThai = () => {
  const [loading, setLoading] = useState({
    province: false,
    district: false,
    subDistrict: false,
  });
  const [province, setProvince] = useState<DefaultOptionType[]>([]); // จังหวัด
  const [district, setDistrict] = useState<DefaultOptionType[]>([]); // อำเภอ / เขต
  const [subDistrict, setSubDistrict] = useState<DefaultOptionType[]>([]); // ตำบล / แขวง
  // const [zipCode, setZipCode] = useState(''); // รหัสไปรษณีย์

  const updateLoading = (key: string, value: boolean) => {
    setLoading((prev) => ({ ...prev, [key]: value }));
  };

  const initProvince = async () => {
    try {
      updateLoading('province', true);
      const resProvince = await service.getProvince();
      setProvince(resProvince);
      setDistrict([]);
      setSubDistrict([]);
    } catch (error) {
      console.error('useAutoCompleteAddressThai:initProvince', error);
    } finally {
      updateLoading('province', false);
    }
  };

  const onSelectProvince = async (provinceId: number) => {
    try {
      updateLoading('district', true);
      setDistrict([]);
      setSubDistrict([]);
      const resDistrict = await service.getDistrictByProvince(provinceId);
      setDistrict(resDistrict);
    } catch (error) {
      console.error('useAutoCompleteAddressThai:onSelectProvince', error);
    } finally {
      updateLoading('district', false);
    }
  };
  const onSelectDistrict = async (districtId: number) => {
    try {
      updateLoading('subDistrict', true);
      setSubDistrict([]);
      const resSubDistrict = await service.getSubDistrictByDistrict(districtId);
      setSubDistrict(resSubDistrict);
    } catch (error) {
      console.error('useAutoCompleteAddressThai:onSelectDistrict', error);
    } finally {
      updateLoading('subDistrict', false);
    }
  };

  useEffect(() => {
    initProvince();
  }, []);

  return {
    loading,
    province,
    district,
    subDistrict,
    onSelectProvince,
    onSelectDistrict,
  };
};

export default useAutoCompleteAddressThai;
