import { DataService } from '@/lib/axios.config';

const MODULE = '/employee';
type Gender = 'Female' | 'Male';

type ProvincesInfo = {
  id: number;
  name_th: string;
  name_en: string;
};

type DistrictInfo = ProvincesInfo & { province_id: number };
type SubDistrictInfo = ProvincesInfo & { zip_code: number; amphure_id: number };

export type EmployeeInfo = {
  id: string;
  first_name: string;
  last_name: string;
  gender: Gender;
  address: string;
  sub_district: number;
  district: number;
  province: number;
  zip_code: string;
  id_card_expiry: Date;
  createdAt: Date;
  updatedAt: Date;
  birthday: Date;
  thai_provinces?: ProvincesInfo;
  thai_amphures?: DistrictInfo;
  thai_tambons?: SubDistrictInfo;
};

type EmployeeWithoutProperties = 'id' | 'createdAt' | 'updatedAt';
export type FilterGetAll = {
  monthOfBirthday?: number;
  cardIdExpiry?: boolean;
};
export type CreatePayload = Omit<EmployeeInfo, EmployeeWithoutProperties>;
export type UpdatePayload = Partial<CreatePayload>;

const convertQueryString = (object: any) => new URLSearchParams(object).toString();

const getAll = async (filter: FilterGetAll) => {
  if (!filter.cardIdExpiry) delete filter.cardIdExpiry;
  if (!filter.monthOfBirthday) delete filter.monthOfBirthday;
  const res = await DataService.get(`${MODULE}?${convertQueryString(filter)}`);
  return res.data;
};

const getById = async (employeeId: string) => {
  const res = await DataService.get(`${MODULE}/${employeeId}`);
  return res.data;
};

const exportCsv = async (filter: FilterGetAll) => {
  const res = await DataService.post(`${MODULE}/export-csv?${convertQueryString(filter)}`);
  return res.data;
};

const create = async (payload: CreatePayload) => {
  const res = await DataService.post(`${MODULE}`, payload);
  return res.data;
};

const update = async (employeeId: string, payload: UpdatePayload) => {
  const res = await DataService.patch(`${MODULE}/${employeeId}`, payload);
  return res.data;
};

const del = async (employeeId: string) => {
  const res = await DataService.delete(`${MODULE}/${employeeId}`);
  return res.data;
};

export { getAll, getById, exportCsv, create, update, del };
