export enum GenderType {
  Female = 'นางสาว',
  Male = 'นาย',
}

export enum FilterCardIdExpiry {
  '' = 'ทั้งหมด',
  'expiry' = 'หมดอายุแล้ว',
  'coming' = 'กำลังจะหมดอายุ',
}

const transferToOptions = (_enum: any) => {
  return (Object.keys(_enum) as Array<keyof typeof _enum>).map((key) => ({
    label: _enum[key],
    value: key as string,
  }));
};

const getGender = () => {
  return transferToOptions(GenderType);
};

const getFilterCardIdExpiry = () => {
  return transferToOptions(FilterCardIdExpiry);
};

export { getGender, getFilterCardIdExpiry };
