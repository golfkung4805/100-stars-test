import { DataService } from '@/lib/axios.config';

const transferToOptions = (arr: any) => arr.map(({ id, name_th }: any) => ({ label: name_th, value: id }));

const MODULE = '/address';

const getProvince = async () => {
  const res = await DataService.get(`${MODULE}/province`);
  return transferToOptions(res.data);
};
const getDistrictByProvince = async (provinceId: number) => {
  const res = await DataService.get(`${MODULE}/district/${provinceId}`);
  return transferToOptions(res.data);
};
const getSubDistrictByDistrict = async (districtId: number) => {
  const res = await DataService.get(`${MODULE}/sub-district/${districtId}`);
  return transferToOptions(res.data);
};

export { getProvince, getDistrictByProvince, getSubDistrictByDistrict, transferToOptions };
