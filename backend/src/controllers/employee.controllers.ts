import { Request, Response, NextFunction } from 'express';
import * as employeeServices from '../services/employee.service';
import type { FilterGetAll, CreatePayload, UpdatePayload } from '../services/employee.service';

type ReqParams = { id: string };

const getAll = async (req: Request<never, never, never, FilterGetAll>, res: Response, next: NextFunction) => {
  try {
    const filter = req.query;
    const employees = await employeeServices.getAll(filter);
    res.json(employees);
  } catch (error) {
    next(error);
  }
};
const getById = async (req: Request<ReqParams>, res: Response, next: NextFunction) => {
  try {
    const _id = req.params.id;
    const employee = await employeeServices.getById(_id);
    res.json(employee);
  } catch (error) {
    next(error);
  }
};
const exportCsv = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const employees = await employeeServices.exportCsv();
    res.header('Content-Type', 'text/csv');
    res.attachment('export-employee.csv');
    res.send(employees);
  } catch (error) {
    next(error);
  }
};
const create = async (req: Request<never, never, CreatePayload, never>, res: Response, next: NextFunction) => {
  try {
    const payload = req.body;
    const createEmployeeInfo = await employeeServices.create(payload);
    res.json(createEmployeeInfo);
  } catch (error) {
    next(error);
  }
};
const updateById = async (req: Request<ReqParams, never, UpdatePayload, never>, res: Response, next: NextFunction) => {
  try {
    const id = req.params.id;
    const payload = req.body;
    const updateEmployeeInfo = await employeeServices.updateById(id, payload);
    res.json(updateEmployeeInfo);
  } catch (error) {
    next(error);
  }
};
const deleteById = async (req: Request<ReqParams>, res: Response, next: NextFunction) => {
  try {
    const id = req.params.id;
    const deleteEmployeeInfo = await employeeServices.deleteById(id);
    res.json(deleteEmployeeInfo);
  } catch (error) {
    next(error);
  }
};

export { getAll, getById, exportCsv, create, updateById, deleteById };
