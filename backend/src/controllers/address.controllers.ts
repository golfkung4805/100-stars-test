import { Request, Response, NextFunction } from 'express';
import * as addressServices from '../services/address.service';

const getProvince = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const province = await addressServices.getProvince();
    res.json(province);
  } catch (error) {
    next(error);
  }
};
const getDistrict = async (req: Request<{ provinceId: number }>, res: Response, next: NextFunction) => {
  try {
    const provinceId = Number(req.params.provinceId);
    const district = await addressServices.getDistrict(provinceId);
    res.json(district);
  } catch (error) {
    next(error);
  }
};
const getSubDistrict = async (req: Request<{ districtId: number }>, res: Response, next: NextFunction) => {
  try {
    const districtId = Number(req.params.districtId);
    const subDistrict = await addressServices.getSubDistrict(districtId);
    res.json(subDistrict);
  } catch (error) {
    next(error);
  }
};

export { getProvince, getDistrict, getSubDistrict };
