import 'dotenv/config';

import express from 'express';

import logger from 'morgan';
import cors from 'cors';
import helmet from 'helmet';

import { employeeRoutes } from './routes/employee.routes';
import { addressRoutes } from './routes/address.routes';
import { errorLogger, errorResponder, invalidPathHandler } from './middlewares/AppError';

const PORT = process.env.PORT;

const app = express();
app.disable('x-powered-by');

app.use(helmet());
app.use(cors({ origin: process.env.CORS?.split(',') ?? '' }));
app.use(express.json());
app.use(logger('combined'));

app.use('/api/employee', employeeRoutes);
app.use('/api/address', addressRoutes);

app.use(errorLogger);
app.use(errorResponder);
app.use(invalidPathHandler);

app.listen(PORT, () => {
  console.log(`[server]: Server is running at http://localhost:${PORT}`);
});
