import { Router } from 'express';
import * as addressControllers from '../controllers/address.controllers';

export const addressRoutes = Router();

addressRoutes.get('/province', addressControllers.getProvince);
addressRoutes.get('/district/:provinceId', addressControllers.getDistrict);
addressRoutes.get('/sub-district/:districtId', addressControllers.getSubDistrict);
