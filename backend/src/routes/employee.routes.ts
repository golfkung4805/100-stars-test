import { Router } from 'express';
import * as employeeControllers from '../controllers/employee.controllers';
import { filterGetAllEmployeeInfo, createEmployeeInfo } from '../middlewares/validator/employee.validator';
import { validationDataResult } from '../middlewares/validator';

export const employeeRoutes = Router();

employeeRoutes.get('/', filterGetAllEmployeeInfo, validationDataResult, employeeControllers.getAll);
employeeRoutes.post('/export-csv', filterGetAllEmployeeInfo, validationDataResult, employeeControllers.exportCsv);

employeeRoutes.get('/:id', employeeControllers.getById);

employeeRoutes.post('/', createEmployeeInfo, validationDataResult, employeeControllers.create);

employeeRoutes.patch(
  '/:id',
  // createOrderServicePaymentInfo,
  // validationDataResult,
  employeeControllers.updateById,
);

employeeRoutes.delete('/:id', employeeControllers.deleteById);
