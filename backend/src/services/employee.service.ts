import { PrismaClient, Employee } from '@prisma/client';
import dayjs from 'dayjs';
import { Parser } from 'json2csv';
import type { FieldInfo } from 'json2csv';

import { AppError } from '../middlewares/AppError';

const prisma = new PrismaClient();

type FilterCardIdExpiry = 'expiry' | 'coming';
type EmployeeWithoutProperties = 'id' | 'createdAt' | 'updatedAt';

export type FilterGetAll = {
  monthOfBirthday?: number;
  cardIdExpiry?: FilterCardIdExpiry;
};
export type CreatePayload = Omit<Employee, EmployeeWithoutProperties>;
export type UpdatePayload = Partial<CreatePayload>;

const getAll = async (filter?: FilterGetAll) => {
  let filterObj = {};
  if (filter?.monthOfBirthday) {
    const month = dayjs().set('month', filter?.monthOfBirthday - 1);
    filterObj = Object.assign(filterObj, {
      birthday: {
        gte: month.startOf('month'),
        lte: month.endOf('month'),
      },
    });
  }
  switch (filter?.cardIdExpiry) {
    case 'coming':
      filterObj = Object.assign(filterObj, {
        id_card_expiry: {
          gte: dayjs(),
          lte: dayjs().add(30, 'day'),
        },
      });
      break;
    case 'expiry':
      filterObj = Object.assign(filterObj, {
        id_card_expiry: {
          lte: dayjs(),
        },
      });
      break;
    default:
      break;
  }
  const employee = await prisma.employee.findMany({
    where: filterObj,
    include: {
      thai_provinces: true,
      thai_amphures: true,
      thai_tambons: true,
    },
  });
  return employee;
};
const getById = async (_id: string) => {
  const employee = await prisma.employee.findUnique({
    where: {
      id: _id,
    },
    include: {
      thai_provinces: true,
      thai_amphures: true,
      thai_tambons: true,
    },
  });
  if (!employee) {
    throw new AppError(404, 'Employee Not Found');
  }
  return employee;
};

const exportCsv = async () => {
  const employees = await getAll();
  const fields: Array<string | FieldInfo<Employee>> = [
    {
      label: 'Gender',
      value: 'gender',
    },
    {
      label: 'First Name',
      value: 'first_name',
    },
    {
      label: 'Last Name',
      value: 'last_name',
    },
    {
      label: 'Birthday',
      value: (row: Employee) => dayjs(row.birthday).format('DD MMMM YYYY'),
    },
    {
      label: 'ID Card Expiry',
      value: (row: Employee) => dayjs(row.birthday).format('DD MMMM YYYY'),
    },
    {
      label: 'Address',
      value: 'address',
    },
    {
      label: 'Provinces',
      value: 'thai_provinces.name_th',
    },
    {
      label: 'District',
      value: 'thai_amphures.name_th',
    },
    {
      label: 'Sub District',
      value: 'thai_tambons.name_th',
    },
    {
      label: 'Zip Code',
      value: 'zip_code',
    },
  ];
  const json2csv = new Parser({ fields, withBOM: true, excelStrings: true });
  const csv = json2csv.parse(employees);
  return csv;
};
const create = async (payload: CreatePayload) => {
  return await prisma.employee.create({
    data: payload,
  });
};
const updateById = async (_id: string, payload: UpdatePayload) => {
  const employee = await getById(_id);
  return prisma.employee.update({
    data: payload,
    where: {
      id: employee?.id,
    },
  });
};
const deleteById = async (_id: string) => {
  const employee = await getById(_id);
  return prisma.employee.delete({
    where: {
      id: employee?.id,
    },
  });
};

export { getAll, getById, exportCsv, create, updateById, deleteById };
