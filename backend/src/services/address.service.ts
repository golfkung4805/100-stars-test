import { PrismaClient } from '@prisma/client';
import { AppError } from '../middlewares/AppError';

const prisma = new PrismaClient();

const getProvince = async () => {
  const provinces = await prisma.thai_provinces.findMany();
  return provinces;
};
const getProvinceById = async (provinceId: number) => {
  const provinces = await prisma.thai_provinces.findMany({
    where: {
      id: provinceId,
    },
  });
  if (!provinces) {
    throw new AppError(404, 'Provinces Not Found');
  }
  return provinces;
};
const getDistrict = async (provinceId: number) => {
  const district = await prisma.thai_amphures.findMany({
    where: {
      province_id: provinceId,
    },
  });
  if (!district) {
    throw new AppError(404, 'Amphures Not Found');
  }
  return district;
};
const getSubDistrict = async (districtId: number) => {
  const subDistrict = await prisma.thai_tambons.findMany({
    where: {
      amphure_id: districtId,
    },
  });
  if (!subDistrict) {
    throw new AppError(404, 'Tambons Not Found');
  }
  return subDistrict;
};

export { getProvince, getProvinceById, getDistrict, getSubDistrict };
