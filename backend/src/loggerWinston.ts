import dayjs from "dayjs";
import { createLogger, format } from "winston";
import DailyRotateFile from "winston-daily-rotate-file";

const transport: DailyRotateFile = new DailyRotateFile({
  filename: "logs/log-%DATE%.log",
  datePattern: "YYYY-MM-DD-HH",
  zippedArchive: true,
  maxSize: "20m",
  maxFiles: "14d",
});

const loggerWinston = createLogger({
  level: "error",
  format: format.combine(
    format((info) => {
      info.level = info.level[0].toUpperCase().concat(info.level.slice(1));
      if (info.stack) {
        info.message = `[${dayjs().format("YYYY-MM-DD HH:mm:ss")}]: ${info.stack} \n`;
      }
      return info;
    })(),
    format.simple()
  ),
  transports: [transport],
});

export default loggerWinston;
