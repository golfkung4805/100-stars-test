import { Request, Response, NextFunction } from 'express';

import { validationResult } from 'express-validator';

const validationDataResult = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new Error(JSON.stringify(errors.array()));
  }
  next();
};

export { validationDataResult };
