import { body, query } from 'express-validator';
import * as addressService from '../../services/address.service';

const filterGetAllEmployeeInfo = [
  query(['monthOfBirthday'], 'format filter month in birthday is incorrect.')
    .isInt({ min: 1, max: 12 })
    .optional({ nullable: true }),
  query(['cardIdExpiry'], 'format filter card id expiry is incorrect.')
    .isIn(['expiry', 'coming'])
    .optional({ nullable: true }),
];
export type EmployeeInfo = {
  sub_district: number;
  district: number;
  province: number;
  id_card_expiry: Date;
  birthday: Date;
};
const createEmployeeInfo = [
  body(['gender'], 'ไม่ได้ส่ง คำนำหน้า').notEmpty().isIn(['Female', 'Male']),
  body(['first_name'], 'ไม่ได้ส่ง ชื่อพนักงาน').notEmpty().isString(),
  body(['last_name'], 'ไม่ได้ส่ง นามสกุลพนักงาน').notEmpty().isString(),
  body(['address'], 'ไม่ได้ส่ง ที่อยู๋').notEmpty().isString(),
  body(['province'])
    .custom(async (province) => {
      await addressService.getProvinceById(province);
    })
    .withMessage('รหัสจังหวัดไม่ถูกต้อง'),
  body(['district'])
    .custom(async (district) => {
      await addressService.getDistrict(district);
    })
    .withMessage('รหัสอำเภอ/เขตไม่ถูกต้อง'),
  body(['sub_district'])
    .custom(async (sub_district) => {
      await addressService.getSubDistrict(sub_district);
    })
    .withMessage('รหัสตำบล/แขวงไม่ถูกต้อง'),
  body(['zip_code'], 'ไม่ได้ส่ง รหัสไปรษณีย์').notEmpty().isString(),
  body(['id_card_expiry'], 'ไม่ได้ส่ง วันหมดอายุบัตรประชาชน').notEmpty().isISO8601().toDate(),
  body(['birthday'], 'ไม่ได้ส่ง วันเกิด').notEmpty().isISO8601().toDate(),
];
export { filterGetAllEmployeeInfo, createEmployeeInfo };
