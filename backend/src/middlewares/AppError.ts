import { NextFunction, Request, Response } from 'express';
import createHttpError from 'http-errors';
import loggerWinston from '../loggerWinston';

export class AppError extends Error {
  statusCode: number;
  rawMessage: string | Record<string, any>;

  constructor(statusCode: number, message: string | Record<string, any>) {
    const errorMessage = typeof message === 'string' ? message : JSON.stringify(message);
    super(errorMessage);

    Object.setPrototypeOf(this, new.target.prototype);
    this.name = Error.name;
    this.statusCode = statusCode;
    this.rawMessage = errorMessage;
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, AppError);
    } else {
      this.stack = new Error().stack;
    }
  }
}

export const errorLogger = (error: AppError, request: Request, response: Response, next: NextFunction) => {
  error.rawMessage = error.message;
  console.error(`errorStack ${error.stack}`);
  loggerWinston.error(error);
  next(error);
};

export const errorResponder = (error: AppError, request: Request, response: Response, next: NextFunction) => {
  response.header('Content-Type', 'application/json');
  const status = error.statusCode || 400;
  response.status(status).send(error.rawMessage);
};

export const invalidPathHandler = (request: Request, response: Response, next: NextFunction) => {
  next(createHttpError(404));
};
